import pytest
from aiohttp import web

from app.api.routes import setup_routes
from app.setup_service.configurator import get_config
from app.setup_service.middlewares import error_middleware


@pytest.fixture
def cli(loop, aiohttp_client):
    """Тестовый клиент aiohttp."""
    app = web.Application(middlewares=[error_middleware])
    setup_routes(app)
    app['config'] = get_config([])
    return loop.run_until_complete(aiohttp_client(app))
