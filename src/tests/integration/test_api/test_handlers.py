from http import HTTPStatus


async def test_handle_without_match_info(cli):
    resp = await cli.get('/')
    assert resp.status == HTTPStatus.BAD_REQUEST.value


async def test_handle(cli):
    resp = await cli.get('/world')
    assert resp.status == HTTPStatus.OK.value
    assert await resp.text() == 'Hello, world'


async def test_version(cli):
    resp = await cli.get('/api/version')
    assert resp.status == HTTPStatus.OK.value

    resp_data = await resp.json()
    assert cli.app['config']['service']['version'] == resp_data['version']
