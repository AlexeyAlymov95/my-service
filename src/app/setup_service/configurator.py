import argparse
from pathlib import Path
from typing import List

from trafaret import Bool, Dict, Int, String
from trafaret_config import commandline

MINIMAL_USERSPACE_PORT = 1001
TRAFARET = Dict({
    'service': Dict({
        'name': String,
        'port': Int(gte=MINIMAL_USERSPACE_PORT),
        'version': String,
        'debug': Bool,
    }),
})


def get_config(args: List[str]) -> dict:
    """Считать конфигурационный файл соответствующий шаблону.

    Иначе сообщить об ошибке в конфигурационном файле.
    """
    ap = argparse.ArgumentParser()
    commandline.standard_argparse_options(
        ap,
        default_config=Path(__file__).parent.parent.parent / 'config' / 'default.yaml',
    )
    options, _ = ap.parse_known_args(args)
    return commandline.config_from_options(options, TRAFARET)
