import logging

from marshmallow import Schema

from app.errors import ServiceError


def response_validation(response_data: dict, schema: Schema) -> None:
    """Валидация ответа сервиса marshmallow схемой."""
    errors = schema.validate(response_data)
    if errors:
        logging.exception({'error': ''}, {})
        raise ServiceError('Server response validation error')
