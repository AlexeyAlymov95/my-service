from http import HTTPStatus

from aiohttp import web, web_exceptions
from aiohttp_apispec import docs, match_info_schema, response_schema
from webargs.aiohttpparser import parser

from app.api.schemas import NameReqSchema, VersionRespSchema
from app.api.validation import response_validation
from app.errors import InvalidRequestError, ServiceError


@docs(tags=['main'], description='Возвращает текст приветствия')
@match_info_schema(NameReqSchema())
async def handle_example(request: web.Request) -> web.Response:
    """Пример обработчика запросов."""
    try:
        request_data: dict = await parser.parse(
            argmap=NameReqSchema,
            req=request,
            locations=('match_info',),
        )
    except web_exceptions.HTTPUnprocessableEntity as validation_error:
        raise InvalidRequestError(validation_error.text)

    name = request_data['name']
    text = f'Hello, {name}'
    return web.Response(text=text)


@docs(tags=['other'], description='Номер версии сервиса')
@response_schema(VersionRespSchema(), HTTPStatus.OK.value)
async def get_version(request: web.Request) -> web.Response:
    """Возвращает номер версии сервиса."""
    config = request.app['config']
    try:
        version: str = config['service']['version']
    except KeyError:
        raise ServiceError('Missing version in config')

    response_data = {'version': version}
    response_validation(response_data, VersionRespSchema())
    return web.json_response(response_data)
